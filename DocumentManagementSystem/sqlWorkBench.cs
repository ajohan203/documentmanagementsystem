﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace DocumentManagementSystem
{
    /*
     * Doc for sqlWorkBench class
     * ---
     * Work bench to extract information from IP3database.
     */
    class sqlWorkBench
    {
        static string dbURL = System.IO.Directory.GetCurrentDirectory() + "/../../db/IP3database.db";

        /*
         * Usage: isUser(username, password);
         *   returns true if username and password match, false otherwise.
         */
        public static bool isUser(string username, string password)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "select * from Users where username = '" + username + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = cmd.ExecuteReader();

            string[] x = new string[2];
            try
            {
                while (reader.Read())
                {
                    x[0] = reader["username"].ToString();
                    x[1] = reader["password"].ToString();
                }

                if (x[0] == username && x[1] == password)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /*
        * Usage: userExists(username, password);
        *   returns true if username exists in database, false otherwise.
        */
        public static bool userExists(string username)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "select * from Users where username = '" + username + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = cmd.ExecuteReader();

            string x = "";
            try
            {
                while (reader.Read())
                {
                    x = reader["username"].ToString();
                }
                if (x == username)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /*
         * Usage: userDetails(username);
         *   returns a list of string which holds information on username in the following way:
         *     [0] = userID
         *     [1] = forename
         *     [2] = surname
         *     [3] = role
         *     [4] = status
         *     [5] = regDate
         */
        public static List<string> userDetails(string username)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "select * from Users where username = '" + username + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = cmd.ExecuteReader();

            List<string> x = new List<string>();
            while (reader.Read())
            {
                x.Add(reader["userID"].ToString());
                x.Add(reader["forename"].ToString());
                x.Add(reader["surname"].ToString());
                x.Add(reader["role"].ToString());
                x.Add(reader["status"].ToString());
                x.Add(reader["regDate"].ToString());
            }
            sqlite.Close();
            return x;
        }

        /*
         * Usage: documentDetails(username);
         *   returns a list of string which holds document information on userID in the following way:
         *     [0] = docID
         *     [1] = title
         *     [2] = surname
         *     [3] = role
         *     [4] = status
         *     [5] = regDate
         *     ...
         */
        public static List<string> documentDetails(string userID)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "select * from Documents where author = '" + userID + "'";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = cmd.ExecuteReader();

            List<string> x = new List<string>();
            while (reader.Read())
            {
                x.Add(reader["docID"].ToString());
                x.Add(reader["title"].ToString());
                x.Add(reader["revNum"].ToString());
                x.Add(reader["author"].ToString());
                x.Add(reader["status"].ToString());
                x.Add(reader["desc"].ToString());
                x.Add(reader["createDate"].ToString());
                //x.Add(reader["actDate"].ToString()); Atli: Having troubles converting the sqlite Date type to string
                x.Add(reader["url"].ToString());
            }
            sqlite.Close();
            return x;
        }

        /*
         * Usage: createUser(username, password, forename, surname, role);
         *   Creates a new user in database wich has password, forename, surname and role.
         */
        public static void createUser(string username, string password, string forename, string surname, string role)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "insert into Users(username, password, forename, surname, role, status) values ('" + username + "', '" + password + "', '" + forename + "', '" + surname + "', " + role + ", 0);";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            cmd.ExecuteNonQuery();
            sqlite.Close();
        }

        /*
         * Usage: deleteUser(username, password, forename);
         *   Deletes user username from database.
         */
        public static void deleteUser(string username)
        {
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
            sqlite.Open();

            string query = "delete from Users where username='" + username + "';";

            SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
            cmd.ExecuteNonQuery();
            sqlite.Close();
        }

        /*
         * Usage: updateDetails(opt, value, username)
         *   Opt is a int between 0-5
         *    0. Updates usernames username to value.
         *    1. Updates usernames password to value.
         *    2. Updates usernames forename to value.
         *    3. Updates usernames surname to value.
         *    4. Updates usernames role to value.
         *    5. Updates usernames status to value.
         */
        public static void updateUserDetails(int opt, string value, string username)
        {
            string query;

            switch (opt)
            {
                case 0:
                    query = "update Users set username='" + value + "' where username='" + username + "';";
                    break;
                case 1:
                    query = "update Users set password='" + value + "' where username='" + username + "';";
                    break;
                case 2:
                    query = "update Users set forename='" + value + "' where username='" + username + "';";
                    break;
                case 3:
                    query = "update Users set surname='" + value + "' where username='" + username + "';";
                    break;
                case 4:
                    query = "update Users set role=" + value + " where username='" + username + "';";
                    break;
                case 5:
                    query = "update Users set status=" + value + " where username='" + username + "';";
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("Invalid selection. Please select 0, 1, 2, 3. 4 or 5.");
                    return;
            }
            try
            {
                SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + dbURL);
                sqlite.Open();

                SQLiteCommand cmd = new SQLiteCommand(query, sqlite);
                cmd.ExecuteNonQuery();
                sqlite.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }
    }
}
