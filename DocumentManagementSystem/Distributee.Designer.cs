﻿namespace DocumentManagementSystem
{
    partial class Distributee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Distributee));
            this.lblDocumentManagement = new System.Windows.Forms.Label();
            this.lblYourDocuments = new System.Windows.Forms.Label();
            this.btnBackToUser = new System.Windows.Forms.Button();
            this.pctbxAddDocument = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDocumentManagement
            // 
            this.lblDocumentManagement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblDocumentManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocumentManagement.Location = new System.Drawing.Point(333, 18);
            this.lblDocumentManagement.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDocumentManagement.Name = "lblDocumentManagement";
            this.lblDocumentManagement.Size = new System.Drawing.Size(862, 97);
            this.lblDocumentManagement.TabIndex = 18;
            this.lblDocumentManagement.Text = "Document Management";
            // 
            // lblYourDocuments
            // 
            this.lblYourDocuments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(182)))), ((int)(((byte)(227)))));
            this.lblYourDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblYourDocuments.Location = new System.Drawing.Point(340, 146);
            this.lblYourDocuments.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblYourDocuments.Name = "lblYourDocuments";
            this.lblYourDocuments.Size = new System.Drawing.Size(358, 43);
            this.lblYourDocuments.TabIndex = 15;
            this.lblYourDocuments.Text = "Available Documents";
            // 
            // btnBackToUser
            // 
            this.btnBackToUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(81)))), ((int)(((byte)(51)))));
            this.btnBackToUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackToUser.Location = new System.Drawing.Point(1184, 705);
            this.btnBackToUser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBackToUser.Name = "btnBackToUser";
            this.btnBackToUser.Size = new System.Drawing.Size(171, 88);
            this.btnBackToUser.TabIndex = 14;
            this.btnBackToUser.Text = "Back";
            this.btnBackToUser.UseVisualStyleBackColor = false;
            // 
            // pctbxAddDocument
            // 
            this.pctbxAddDocument.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pctbxAddDocument.BackgroundImage")));
            this.pctbxAddDocument.Location = new System.Drawing.Point(348, 218);
            this.pctbxAddDocument.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pctbxAddDocument.Name = "pctbxAddDocument";
            this.pctbxAddDocument.Size = new System.Drawing.Size(168, 235);
            this.pctbxAddDocument.TabIndex = 27;
            this.pctbxAddDocument.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::DocumentManagementSystem.Properties.Resources.ideagenLogo;
            this.pictureBox1.Location = new System.Drawing.Point(18, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(306, 298);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // Distributee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(50)))), ((int)(((byte)(84)))));
            this.ClientSize = new System.Drawing.Size(1389, 825);
            this.Controls.Add(this.pctbxAddDocument);
            this.Controls.Add(this.lblDocumentManagement);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblYourDocuments);
            this.Controls.Add(this.btnBackToUser);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Distributee";
            this.Text = "Distributee";
            this.Load += new System.EventHandler(this.Distributee_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pctbxAddDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDocumentManagement;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblYourDocuments;
        private System.Windows.Forms.Button btnBackToUser;
        private System.Windows.Forms.PictureBox pctbxAddDocument;
    }
}