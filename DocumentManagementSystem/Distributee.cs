﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManagementSystem
{
    public partial class Distributee : Form
    {
        private User currentUser;

        public Distributee(User currentUser)
        {
            InitializeComponent();
            this.currentUser = currentUser;
        }

        private void Distributee_Load(object sender, EventArgs e)
        {
            List<string> docs = sqlWorkBench.documentDetails(currentUser.userID);

            // Set initial coordinations of buttons created for each document.
            int x = 360;
            int y = 142;

            /* Creates a new button for each document.
             * Each button has its text as the documents title and description.
             * If button is pressed a save dialog pops up where you chose the location where you want
             * to save file. File is then saved.
             */
            for (int i = 0; i < docs.Count(); i += 8)
            {
                Button btn = new Button();
                btn.Location = new Point(x, y);
                btn.Margin = new Padding(4, 5, 4, 5);
                btn.Size = new Size(112, 153);
                btn.TabIndex = 33;
                btn.UseVisualStyleBackColor = true;
                btn.BackColor = Color.FromArgb(139, 182, 227);
                btn.ForeColor = Color.Black;
                string info = "Title:\n" + docs[i + 1] + "\n\nDescription:\n" + docs[i + 5];
                btn.Text = info;

                string url = @docs[i + 7];
                string filename = Path.GetFileName(url);

                btn.Click += delegate
                {
                    SaveFileDialog sfd = new SaveFileDialog();

                    sfd.Filter = "All files (*.*)|*.*";
                    sfd.FilterIndex = 2;
                    sfd.RestoreDirectory = true;
                    sfd.FileName = filename;
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        string newDir = sfd.FileName;
                        if (File.Exists(newDir))
                            File.Delete(newDir);
                        File.Copy(url, newDir);
                    }
                };
                Controls.Add(btn);
                x += 130;
            }
        }
    }
}
